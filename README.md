Ok, so my aim is to make installation a (somewhat) breeze if you're not familiar with java projects. This game should be played by anyone, so I'd like to open it up to java newbs too.

I'm going to walk through my install, which took some time to figure out, but is now mostly working. There seem to be some missing audio files, so I'll work on finding those and incorporating them.

First order of business, however, is to thank Thomas Foote for a huge effort porting this DOS game from 1985 to JAVA so we can keep playing it. THANK YOU!

Next: assumptions about your environment.
 - You are on a Mac - first and foremost - these instructions are not for PC
 - You have homebrew installed - if not, get it here http://brew.sh/
 - You have java installed - type `which java` - if it comes back with a directory, you're golden

Now the meat:

1. Install Maven - a java project tool: `brew install maven`
2. 






20. Final step is to run `java -cp target/dq-1.1-SNAPSHOT.jar com.droidquest.DQ`



DroidQuest
==========

A Java recreation of the classic game Robot Odyssey I


Copyright (c) 2000 Thomas Foote

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
